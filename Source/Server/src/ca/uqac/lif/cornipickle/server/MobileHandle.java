package ca.uqac.lif.cornipickle.server;


import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.Map;
import java.util.Set;

import javax.xml.bind.DatatypeConverter;

import com.sun.net.httpserver.HttpExchange;

import ca.uqac.lif.cornipickle.Interpreter;
import ca.uqac.lif.cornipickle.Verdict;
import ca.uqac.lif.cornipickle.Interpreter.StatementMetadata;
import ca.uqac.lif.jerrydog.CallbackResponse;
import ca.uqac.lif.jerrydog.RequestCallback;
import ca.uqac.lif.json.JsonElement;
import ca.uqac.lif.json.JsonList;
import ca.uqac.lif.json.JsonMap;
import ca.uqac.lif.json.JsonNumber;
import ca.uqac.lif.json.JsonParser;
import ca.uqac.lif.json.JsonString;
import ca.uqac.lif.json.JsonParser.JsonParseException;

public class MobileHandle extends InterpreterCallback {
	static JsonParser s_jsonParser;

	public MobileHandle(Interpreter i) {
		// super(i, m, path);
		// TODO Auto-generated constructor stub
		// System.out.print("test connection etablie");
		super(i, RequestCallback.Method.POST, "/mobiletest/");
		s_jsonParser = new JsonParser();

	
	}

	@SuppressWarnings("deprecation")
	@Override
	public CallbackResponse process(HttpExchange arg0) {
		JsonElement j = null;
		Map<String, String> attributes = getParameters(arg0);
		System.out.println("json : " + attributes.toString());
		System.out.println("json content : " + attributes.get("contents"));
		// j = s_jsonParser.parse();
		try {
			
			j = s_jsonParser.parse(URLDecoder.decode(attributes.get("contents"),"UTF-8"));
			System.out.println("j : " + j.toString());
			// s_jsonParser.parse(attributes.toString().substring(2,attributes.toString().length()-1
			// ));
			if (attributes.get("interpreter") != null) {
			
				m_interpreter = m_interpreter.restoreFromMemento(URLDecoder.decode(attributes.get("interpreter"),"UTF-8"));
			//	m_interpreter = m_interpreter.restoreFromMemento(attributes.get("interpreter").toString());
				System.out.println("passe");
			}
		} catch (JsonParseException e) {
			System.out.println("Exception");
			e.printStackTrace(); // Never supposed to happen....

		} /*catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/ catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (j != null) {
			m_interpreter.evaluateAll(j);
			
			// m_server.setLastProbeContact();
			System.out.println("evaluate");
		}
		System.out.println("null");
		// Select the dummy image to send back
		Map<StatementMetadata, Verdict> verdicts = m_interpreter.getVerdicts();
		// byte[] image_to_return = selectImage(verdicts);
		// Create response
		CallbackResponse cbr = new CallbackResponse(arg0);
		cbr.setHeader("Access-Control-Allow-Origin", "*");
	    cbr.setContents(createResponseBody(verdicts, m_interpreter.saveToMemento()));
		cbr.setContentType(CallbackResponse.ContentType.JSON);
	//	cbr.setContents("response from server");

		System.out.println("fin json mobile : " + j.toString());
	
		return cbr;

	}

	/**
	 * Creates the response body out of the verdict of each property handled by
	 * the interpreter. This response contains, for each property, its caption,
	 * a list of element IDs to highlight (if any), as well as the total number
	 * of true/false/inconclusive properties. In addition, it contains the
	 * serialized state of the current interpreter
	 * 
	 * @param verdicts
	 *            A map from the metadata for each property to its current
	 *            verdict
	 * @param interpreter
	 *            String in base 64 representing the state of the interpreter
	 * @param image
	 *            The image to return as an array of bytes
	 * @return A JSON string to be passed as the response to the browser
	 */
	protected static String createResponseBody(Map<StatementMetadata, Verdict> verdicts, String interpreter) {
		int num_false = 0;
		int num_true = 0;
		int num_inconclusive = 0;
		Verdict outcome = new Verdict(Verdict.Value.TRUE);
		JsonList highlight_ids = new JsonList();
		for (StatementMetadata key : verdicts.keySet()) {
			JsonMap element = new JsonMap();
			JsonList id_to_highlight = new JsonList();
			Verdict v = verdicts.get(key);
			outcome.conjoin(v);
			if (v.is(Verdict.Value.FALSE)) {
				num_false++;
				id_to_highlight.addAll(getIdsToHighlight(v));
			} else if (v.is(Verdict.Value.TRUE)) {
				num_true++;
			} else {
				num_inconclusive++;
			}
			element.put("ids", id_to_highlight);
			element.put("caption", new JsonString(CornipickleServer.escapeQuotes(key.get("description"))));
			highlight_ids.add(element);
		}
		JsonMap result = new JsonMap();
		result.put("global-verdict", outcome.toPlainString());
		result.put("num-true", num_true);
		result.put("num-false", num_false);
		result.put("num-inconclusive", num_inconclusive);
		result.put("highlight-ids", highlight_ids);
		result.put("interpreter", interpreter);
		// result.put("image", "data:image/png;base64," +
		// DatatypeConverter.printBase64Binary(image));
		// Below, we use true to get a compact JSON string without CF/LF
		// (otherwise the cookie won't be passed correctly to the browser)
		return result.toString("", true);
	}

	protected static JsonList getIdsToHighlight(Verdict v) {
		JsonList ids = new JsonList();
		Set<Set<JsonElement>> tuples = v.getWitness().flatten();
		for (Set<JsonElement> tuple : tuples) {
			JsonList out = new JsonList();
			for (JsonElement e : tuple) {
				if (!(e instanceof JsonMap)) {
					continue;
				}
				JsonMap m = (JsonMap) e;
				JsonElement id = m.get("cornipickleid");
				if (id == null || !(id instanceof JsonNumber)) {
					continue;
				}
				out.add((JsonNumber) id);
			}
			ids.add(out);
		}
		return ids;
	}

	/***
	 * 
	 * 	System.out.println("jk : " + attributes.get("interpreter"));
				System.out.println(" jk1 "+(URLDecoder.decode(attributes.get("interpreter"),"UTF-8")+"="));
				 System.out.println("nombre: "+URLDecoder.decode(attributes.get("interpreter"), "UTF-8").length());
				 
				 // .substring(2,attributes.toString().length()-1 )
			// j = s_jsonParser.parse(attributes.toString());
			// if(attributes.get("interpreter") != null)
			/// {
			// m_interpreter =
			// m_interpreter.restoreFromMemento("eJztXetS4zgWfhWPN1XbF8hC985sDbXbUwwdZpii0ywB+keHpRRbSTQolluSgdDFY+1r7DOtJDuJc8UOUqI4/jFTjS/Sp3P5zpF05Hx3ezcM8o+wzdyD7+4PHgZM/Mv9E9yBasQRrv4OWPcTCN0dcZMEHAZc3P96/bQj3oR3AEeAwzp84OIlTiPoyuuMi4u9+NEcjYpHZYNw7CVPvPINeFWM2lWP0ACFyLvFsHok3xPdjjXxXXSOggDSxgBBprbOKPSRJ944AhhPtxgObsd/0gjDOuglfw0Ry2FTFHREP5eN2vnH2vH55WntnfukRDXWxrOAPomHwb8jSPujgUzBuoNUNMnjP+4RDyBjF3QAxusi7FMYzNHAKQpuoX+KGJ+tWZxIL4gwVleSDo4BZoZ6SCTpntSPPtePTi8bJ1c1V93pDcUhbr/qoWD3Hvm8e+C829sLH167488ckUj0Rd2DfXkd+e7BXqIDwMX1YL7eeBc6IehABzGnhTpOoruiCvri/DIRsAdCHlH4GyVRqN7cSew8/da2SOFXTLzboRR6gHtd6Dfm2EjBTWTcF+VYf7iF/UwcdiKdUNCe+H9lyGKfIAc+4CAL8yv4GASdaiL8sXfc9+4sNM+8FAXoWwQFJzw9XYvR5Io4x4QeYryIkAFFoIUXBIbKY2IvSwQpHd3z5bv/TMtQNJHafF1varG3VGpR+xYJuS3QJUWdbjaTqMVyO6MkhJT3zwhjQi3oDs4aZ/xMPDgXwzZ3U6JPLlf6yZDU/fVAeNg6Rs+QGFUeHBD4TqXvAAodKbpdgFEngH7Rc6RpBpjMlbIHH2lbOzke7+d7PH/r25TmzVXkMN3LIbvHfKLmiagnc8nKY+xWfMqt3KexDtYQXfY3N7pwEq45uMxBUMaWZ2OLkFwZWsrQUgxFWhNaUl6VzHynJoEsHphCH3HSbl8lozs+PG3UFJv5pAdQMHpue/QpZn2UX6UXjNRVmC2OHDHWEL15nNDpyOGlbh64VQEpcjCS7FkqaTOUZM2K2CcBizmj6Jl/eSyQicq8dbFn3j3EWMiEcQeJgTGHdUmEfQci3oXUaSXprUNomoyqS6H0IfMoCjkiwbJg//7yxcPrdExj5ja6rhIHnjLKDXLmDMP8Enc2w/fKPss+t6jPCW+cA+FPRoLqH+J/9ajXgjNCVhBfn01vp2SS3H76+UlFs+0Z8T/iEc8iv9kX1yKbbelzPTaw/+P7bTP7/Z/3c9p9lovPgV8iBxq+OysXG1tBUpl4/nSOg87CfNekFt7Pnjk8gzhljshfC+7YepaQNQnXgleRfH64aok4F97sNLoanzj6eHhxuDq3eBbPYUDURFBNneUMcTlssuJPks/1Mt4z4Gk1fStpzQQ9bCqt/bgcIa+L1n4qac0OWjtGlHFLSE3nuNQ2reNh4ZRLjQneSf6cT7Tpldx47bpcSd3yldQQUBZTYKroQNVyi3R5rBLhI2yjAEnQzNyy69l0bzOqGcqjACve5CmPAqypFmFyQ38Jr5/0hTX7Y1nhVFY4lRVOZYXTkq3rJcR9CwixPFBQHigoDxSUnGgLJ+65M+eGgHIE8Jm4FNtSWylt0POCJaqWgHQv9Cbc/NegfS5hTjm1NMXfhWk2kJ8YQhD78T8bH2J1ASxNG3DBCfOmn3l3VlPALsgtnLk8k+HdOgkuIBVzIoBVMzOWGZOxDBn+QyxiW3D5sC0L9ixDNTzzZxkudfUhQXW9YBv7ZYYP8Q2hNwHh1jtANrkNuHkFuqyFDOGs+vzffw0rUniW+rf1eszhAowJwogrcy3zzkhELDv5jMIOfBikHabZY1wMRbG7OJE1Lbs7QDfDYc+lRWUT3X+azcrXZvO+2fSv3xqW3wQ72C3DbOKrvFpwNPQlFHozuPshWwfZWn+9Gg2nwdut5fx6kfOdjDp5mdItilCTY1+FCSV92W0+eYhWsuxfms1qs/mm2dxdCdkCLobVirj9ESubDAPiw/hE2QrcIxukO7vgdKFa3bUGj9oNtgdOXCBmCZik/MsSNNQuw2kRzknPHjwewYTaAyep1bECy7Bgzgo0LeDddiiJAosk1CJizm2R9YzKAa2AM1lwbAksH7EQg749gBh6tCjVAJ4HGZPbSdZA8rrQu4V2mZD8BKRFiMQjhiceskyvcDOPy/NTe3RYJvmLiYmF0OO7VFgbsQcVoUhcA0ltt0kH9OEd8orngnaq1V5fNLkDDUflVtZbV/ZlzrFx3YTEtmXYcXwe6ZkvNZgWSFGUndrM07e/8ldmYmcgzeS28AzvQp2Ck0mjfuFNpaIFlV4c8fXLb0YmsUK+iQnObr7RrO4X8II2YyJt3VjTVLtpDvgSslm5StZG3YVW7CwW3A7VxiM3TPqT5W9FIHz1+V/ddjg57dKn53vEtaNdTUleuhbcbsPJLrk2oRB4Xdsmnw9CYrbNiFviGdq/YbxnW1FvZCuwAHbUIqRlsIQXh4QCbKPIhjXT9kHD1lWz33ehbbalPkDxTX6AIqVCczFpuju7I1M2KV7oTS2VkHQ2qARuLonRhhOEIUaQGbbB8cBot/3lyEHk2XYUJ78W8UvHNg7GtgGK53hqVmQZMuE9liGy7uQc6inGMn4yrYiM5cuPgUDfeMoBeLdAUgO+bU7JopZliHx0ZxmiXoSN59aT08SiWHwHkxbA2LbMRhUKR1YDk9+K6SLbppyBAGhbHA+GH6ewCtINRz3jO8ujNVW7GSOb3I7VURRtU1IpGZN7vtqAokD7BH/0DQ59MLWfCR/74s3GHAYf7hgUweUuupBqXQCLpVO6nT6YLMpKY1k30wEvaUE7LSQbBEUghS9iKJvhwRmJJusx/PuNGHXpcKk9wiL42ynMSMhrNryW/uLCQVEsofIjMUyvBZaOktoLKIKjbJBCtTV2GPilT2j3icJ8qW2D9Kmtsc9al4k2SIImPWK0HVoEtzjRWn++QSairbGLzJPAwsnQpJul6lSL4Gd1Uq4l6TeS1LZpEYzkEN+DvtalmtJOxrZji2AkdTmW0kYM2MhDUdbJ6vEX+UoL0WshY9UwRbCT2nBApbUYtJZRiVJpNPNajUWk23Li33HXu8sMxWW/TNLMBOBBMVgRHEXzialAc1CXkt5SKy7Xfuxmglm7nXYzwgu+4mBROa6SNbDuh/WS0y1ms7XR0cKiWJrZmgElMP1fbJuF2SJTNCvTrGVaL5anwRXZInG1HdrO1lqHQhHqtS4M8i7QPiNbuUHi0iDXY5AYMq3tFcIaxw/Cl2aZpXklLbj5oXF0Frwoejf3RTq9VJRI3nT5qG+/bte0DGGWIN5uFlzTqyfxtwpKO1y5Ync3C65pO0y+B1Ea4so1+2az4Jo2xPhTKaUdrlyxzb9tFl7jP48+Wl+32xqX2WG3aJV2Nd+eHg7dbl3m+Y3yZtM3/bvkQ90USGruV/Hf9Zuma34F4YYV5jTvoe7gkIhH/W3xagQItFZO6f+9loEc5fKOvLuChbG03uy27TzEUH3zy6tf/oWY8QR3WmF2C3FpW9Tq1jurcRxr0qIZslyhVRbIKL2xOhmTdVDQRx4ozE+4ftFacMiA1jia/XNPeaar0L8JAZdq0/uLPWXhoSmHS+nLbp/Ln4lIqzEtwokfS7FbhNnt+bJRO/9YOz6/PK29syysp6Dt2wttbyXQMtZKBYC2gzag5tf7eHFOFWo+z9AmGJN7CVdjo1RoSO9kPQw1H3QqI7cJV5tgGrv9LU/kftVsVr42m/fNpn/99rUjBu9MXqLQkYLZBRh1zJe5TISbbZM0J+HqBf2uUILmXejIXw12EHNaqDMU47UcoqAfypWonhFPw3qZ5JsnDib6NuVvow0Hm1CNRSiLcI3SvQEvyJvEhws68ggNUIg82dMxoYcYNwajm+7oDlAEWgPXuAM4Sv4Zb6qJpiuPbtwtCgJIR00drKh7vnz3n+mirsWkEnk8/uMe8QAydkEH/XtdhH0qVJPT/a8V1mR90j0IhNLVlaSDY4CZoR4S4bkn9aPP9aPTy8bJVS2ms6FxZzSaswF1HAGMpwU3ZJb4T5kf15W5zlNhOpFKlDnWxrOAaupg2AJdUtTpZjOJWiy3s6To44wwJtQiiH7WOONn6oPDB23Z81D0yeVKPxmSur8eCA8JhO0w6cSC4iW1BcTxoPKtSn8qm3W2SlzyjgdCHlH4GyVROEECqj8Mgk51VjBTtrWT4/F+vsfzt64yOqqSudHAt02Rv2Li3eZU5GM+UfNE1PFRGb8xSgdit+KzJonz8qYVRZf9zY0uYg645uAyB0EZW56NLan5exlaytCy2Yq0JrRMrorNmASyeGAKfcRJu32VjO748LRRU2zmkx5Awei57dGnWvUaCOTi/DKWB4PZ4sgRYw3Rm8cJnY4cXurmgVsVkCIHI8mepZI2QUnCmf4PFli1AQ==");
			// m_interpreter =
			// m_interpreter.restoreFromMemento(attributes.get("contents"));
			//
			// }
	 * 
	 * */
	
	 
}
